from django.urls import path
from accounts.views import user_login, user_signup, user_logout

urlpatterns = [
    # path("", recipe_list, name='recipe_list'),
    path("signup/", user_signup, name='user_signup'),
    path("login/", user_login, name='user_login'),
    path("logout/", user_logout, name='user_logout'),
]
